import glob
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import pickle
import shutil
import torch
from PIL import Image
from pathlib import Path
from torchvision import transforms
import math
import torch.nn.functional as F

class data_generator:
    def __init__(self, args, min_bees_per_image=119, max_bees_per_image=183):

        self.angle_file = args.angle_file

        self.front_images = []
        self.synthetic_data_directory = args.synthetic_data

        self.bee_masks = []

        self.bee_sizes = []
        self.min_bees_per_image = min_bees_per_image
        self.max_bees_per_image = max_bees_per_image

        self.pickle_file_paths_full_data = []
        self.pickle_file_paths_truncated_data = []

        self.num_bees = None

        self.x_angle = None
        self.y_angle = None

        self.convert_tensor = transforms.ToTensor()

        self.convert_tensor = transforms.ToTensor()

        self.convert_pil = transforms.ToPILImage()

        for filename in glob.glob(f"{args.bee_cutouts}/*.png"):
            frontImage = Image.open(filename).convert("RGBA")
            self.front_images.append(frontImage)

        self.background_images = []
        for filename in glob.glob(f"{args.background}/*.jpg"):
            frontImage = Image.open(filename).convert("RGBA")
            self.background_images.append(frontImage)
        for filename in glob.glob(f"{args.background}/*.png"):
            frontImage = Image.open(filename).convert("RGBA")
            self.background_images.append(frontImage)

        self.get_background_masks(args.cut_images)
        self.num_bees = len(self.front_images)

        self.largest_bee_size = 120

    def get_background_masks(self, cut_images):
        angle_data = pd.read_csv(self.angle_file).to_numpy().flatten()
        self.x_angle = np.cos(angle_data*np.pi/180)
        self.y_angle = np.sin(angle_data*np.pi/180)
        for i, image in enumerate(self.front_images):
            front_image_numpy = np.array(image)
            x_cords, y_cords = np.where(front_image_numpy[:, :, -1] != 0)
            min_x = min(x_cords)
            max_x = max(x_cords)
            min_y = min(y_cords)
            max_y = max(y_cords)
            self.bee_masks.append(np.array([x_cords - min_x, y_cords - min_y]))
            cut_image = front_image_numpy[min_x:max_x, min_y:max_y]
            cut_image = Image.fromarray(cut_image)
            cut_image.save(f"{cut_images}/{i}.png")
            self.front_images[i] = cut_image.convert("RGBA")
            self.bee_sizes.append( [max_x-min_x, max_y-min_y] )
        self.bee_sizes = np.array(self.bee_sizes)

    def get_cutout_stats(self):
        dataframe = pd.DataFrame([self.bee_sizes[:,0], self.bee_sizes[:, 1], self.bee_sizes[:,0]*self.bee_sizes[:,1]]).T
        dataframe.columns = ['widths', 'heights', 'size']
        return dataframe.describe()

    def place_bees(self, background_image, bees_to_place, full_y_data=True):
        x_max, y_max = background_image.size
        staple_individual_bee_masks = np.zeros((y_max, x_max))
        bee_ids = []
        box = []
        bee_center = []
        individual_bee_masks_list = []
        summed_mask = torch.tensor(staple_individual_bee_masks)

        for i in range(bees_to_place):
            bee_id = np.random.randint(0, self.num_bees)
            bee_image = self.front_images[bee_id]
            placement_not_accepted = True

            while placement_not_accepted:
                x_cord = np.random.randint(low=0, high=x_max)
                y_cord = np.random.randint(low=0, high=y_max)
                bee_mask = self.bee_masks[bee_id].copy()
                bee_mask[0] += y_cord
                bee_mask[1] += x_cord
                max_for_covering = np.logical_and(bee_mask[0] < y_max, bee_mask[1] < x_max)
                bee_mask = bee_mask[:, max_for_covering]
                if torch.sum(summed_mask[bee_mask[0], bee_mask[1]]) == 0:
                    placement_not_accepted = False

            background_image.paste(bee_image, (x_cord, y_cord), bee_image)

            individual_bee_masks = staple_individual_bee_masks.copy()
            individual_bee_masks[bee_mask[0], bee_mask[1]] = 1

            individual_bee_masks_list.append(torch.tensor(individual_bee_masks))

            if full_y_data:
                x_size, y_size = bee_image.size
                bee_center.append([ y_cord+ y_size//2, x_cord + x_size//2 ])
                box.append([y_cord, x_cord, y_size, x_size])
                bee_ids.append(bee_id)

            summed_mask += individual_bee_masks_list[-1]

        background_image = self.convert_tensor(background_image)[:3,:,:]

        if bees_to_place > 0:
            individual_bee_masks = torch.stack(individual_bee_masks_list)
        else:
            individual_bee_masks = []


        if full_y_data:
            bee_sizes = self.bee_sizes[bee_ids]

            x_angle = self.x_angle[bee_ids]
            y_angle = self.y_angle[bee_ids]

            return background_image, bee_ids, individual_bee_masks, box, summed_mask, np.array([x_angle, y_angle]).T
        return background_image, summed_mask


    def clean_dirctory(self, directory_path):
        if os.path.exists(directory_path):
            for files in os.listdir(directory_path):
                if Path(directory_path).exists():
                    if Path(directory_path).is_dir():
                        shutil.rmtree(directory_path)
                    else:
                        os.remove(f"{directory_path}/{files}")
            if Path(directory_path).exists():
                os.rmdir(directory_path)
        os.mkdir(directory_path)

    def generate_data_for_image(self, num_images, num_bees, full_y_data=True):
        background_image_id = np.random.randint(0, len(self.background_images))
        if num_images > 1:
            background_image_list, individual_bee_masks_list, box_list, summed_mask_list, bee_angle_list, bee_id_list = [], [], [], [], [], []
            for i in range(num_images):

                if full_y_data:
                    background_image, bee_ids, individual_bee_masks, box, summed_mask, bee_angles = self.place_bees(self.background_images[background_image_id].copy(), num_bees, full_y_data)
                    background_image_list.append(background_image)
                    individual_bee_masks_list.append(individual_bee_masks)
                    box_list.append(torch.tensor(box))
                    summed_mask_list.append(summed_mask)
                    bee_angle_list.append(torch.tensor(bee_angles).float())
                    bee_id_list.append(bee_ids)
                else:
                    background_image, summed_mask = self.place_bees(self.background_images[background_image_id].copy(), num_bees, full_y_data)
                    background_image_list.append(background_image)
                    summed_mask_list.append(summed_mask)

            if full_y_data:
                return torch.stack(background_image_list), {'individual_bee_masks': individual_bee_masks_list,
                        'box': torch.stack(box_list),
                        'summed_mask': torch.stack(summed_mask_list),
                        'bee_angles': torch.stack(bee_angle_list),
                        'background_image_id':background_image_id,
                        'bee_id':bee_id_list}
            else:
                return torch.stack(background_image_list),{'summed_mask': torch.stack(summed_mask_list)}
        else:
            if full_y_data:
                background_image, bee_ids, individual_bee_masks, box, summed_mask, bee_angles = self.place_bees(self.background_images[background_image_id].copy(), num_bees, full_y_data)

                return torch.tensor(background_image).unsqueeze(0), {'individual_bee_masks': individual_bee_masks,
                        'box': torch.tensor(box).unsqueeze(0),
                        'summed_mask': summed_mask.unsqueeze(0),
                        'bee_angles': torch.tensor(bee_angles).unsqueeze(0),
                        'background_image_id':background_image_id,
                        'bee_id':[bee_ids]}
            else:
                background_image, summed_mask = self.place_bees(self.background_images[background_image_id].copy(), num_bees, full_y_data)
                return torch.tensor(background_image).unsqueeze(0), {'summed_mask': torch.tensor(summed_mask).unsqueeze(0)}

    def get_rot_mat(self, theta):
        theta = torch.tensor(theta)
        return torch.tensor([[torch.cos(theta), -torch.sin(theta), 0],
                             [torch.sin(theta), torch.cos(theta), 0]])

    def rot_img(self, x, theta):
        rot_mat = self.get_rot_mat(theta)[None, ...].repeat(x.shape[0], 1, 1)
        grid = F.affine_grid(rot_mat, x.size())
        x = F.grid_sample(x, grid)
        return x

    def make_movements(self, background_image, boxes, angles, bee_ids):

        x_max, y_max = background_image.size
        staple_individual_bee_masks = np.zeros((y_max, x_max))

        individual_bee_masks_list = []
        summed_mask = torch.tensor(staple_individual_bee_masks)

        actual_angle_degrees = (np.arccos(angles[:,0]) * (180 / np.pi) * np.sign(angles[:,1]) + 360) % 360

        box = []

        x_angle_list = []
        y_angle_list = []

        for i in range(len(boxes)):
            bee_id = bee_ids[i]

            placement_not_accepted = True

            bee_image = self.front_images[bee_id]
            image_tensor = self.convert_tensor(self.front_images[bee_id])

            tries = 0

            while placement_not_accepted and tries < 5:

                new_rotation = torch.randint(-5, 5, (1,))[0] + actual_angle_degrees[i]
                translation = torch.randint(0, 10, (1,))[0]

                if tries > 3:
                    translation = 0

                x_displacement = np.cos(new_rotation * (np.pi / 180)) * translation
                y_displacement = np.sin(new_rotation * (np.pi / 180)) * translation

                x_cord = boxes[i][0] + x_displacement.long()
                y_cord = boxes[i][1] + y_displacement.long()
                bee_mask = torch.tensor(self.bee_masks[bee_id].copy())
                bee_mask[0] += y_cord
                bee_mask[1] += x_cord
                max_for_covering = torch.logical_and(bee_mask[0] < y_max, bee_mask[1] < x_max)
                bee_mask = bee_mask[:, max_for_covering]
                if torch.sum(summed_mask[bee_mask[0], bee_mask[1]]) == 0:
                    placement_not_accepted = False
                tries += 1

            x_angle_list.append(np.cos(new_rotation * (np.pi / 180)))
            y_angle_list.append(np.sin(new_rotation * (np.pi / 180)))

            background_image.paste(bee_image, (x_cord, y_cord), bee_image)
            individual_bee_masks = staple_individual_bee_masks.copy()
            individual_bee_masks[bee_mask[0], bee_mask[1]] = 1
            individual_bee_masks_list.append(torch.tensor(individual_bee_masks))
            summed_mask += individual_bee_masks_list[-1]

            x_size, y_size = bee_image.size
            box.append([y_cord, x_cord, y_size, x_size])

        overlap = torch.where(summed_mask > 1)

        if len(overlap[0]) > 0:
            summed_mask[overlap[0],overlap[1]] = 1

        individual_bee_masks = torch.stack(individual_bee_masks_list)
        background_image = self.convert_tensor(background_image)[:3, :, :]
        return background_image, bee_ids, individual_bee_masks, box, summed_mask, np.array([x_angle_list, y_angle_list]).T


    def evolve_input(self, target):
        boxes = target['box']
        bee_angles = target['bee_angles']
        background_image_id = target['background_image_id']
        bee_ids = target['bee_id']

        background_image_list, individual_bee_masks_list, box_list, summed_mask_list, bee_angle_list, bee_id_list = [], [], [], [], [], []
        for i in range(len(boxes)):
            background_image, bee_ids, individual_bee_masks, box, summed_mask, bee_angles = self.make_movements(
                self.background_images[background_image_id].copy(), boxes[i], bee_angles[i], bee_ids[i])

            background_image_list.append(background_image)
            individual_bee_masks_list.append(individual_bee_masks)
            box_list.append(torch.tensor(box))
            summed_mask_list.append(summed_mask)
            bee_angle_list.append(torch.tensor(bee_angles).float())
            bee_id_list.append(bee_ids)

        individual_bee_masks = torch.stack(individual_bee_masks_list)[0]
        box = torch.stack(box_list)
        summed_mask = torch.stack(summed_mask_list).unsqueeze(0)
        bee_angles = torch.stack(bee_angle_list)

        assert target['individual_bee_masks'].shape == individual_bee_masks.shape
        assert target['box'].shape == box.shape
        assert target['summed_mask'].shape == summed_mask.shape
        assert target['bee_angles'].shape == bee_angles.shape
        assert target['background_image_id'] == background_image_id
        # assert target['bee_id'].shape == bee_id_list

        return torch.stack(background_image_list), {'individual_bee_masks': individual_bee_masks,
                                                    'box': box,
                                                    'summed_mask': summed_mask,
                                                    'bee_angles': bee_angles,
                                                    'background_image_id': background_image_id,
                                                    'bee_id': bee_id_list}


    def preprocess_synthetic_data(self, num_images):
        self.clean_dirctory(f"{self.synthetic_data_directory}/images")
        self.clean_dirctory(f"{self.synthetic_data_directory}/annotations")
        os.mkdir(f"{self.synthetic_data_directory}/annotations/full_annotations")
        os.mkdir(f"{self.synthetic_data_directory}/annotations/summed_mask_only")

        data_collection = {"bee_masks": self.bee_masks,
                           "bee_sizes": self.bee_sizes}

        with open(f"{self.synthetic_data_directory}/annotations/synthetic_data_bees.pkl", 'wb') as f:
            pickle.dump(data_collection, f)

        pickle_file_paths_full_data = []
        pickle_file_paths_truncated_data = []

        for synthetic_id in range(num_images):
            num_bees = np.random.randint(self.min_bees_per_image, self.max_bees_per_image)

            background_image, individual_bee_masks, bee_center, summed_mask, bee_sizes = self.generate_data_for_image(1, num_bees)

            self.convert_pil(background_image).save(f"{self.synthetic_data_directory}/images/{synthetic_id}.png", format="png")

            # background_image.save(f"{self.synthetic_data_directory}/images/{synthetic_id}.png", format="png")

            data_collection = {"image_directory":f"{self.synthetic_data_directory}/images/{synthetic_id}.png",
                              'individual_bee_masks':individual_bee_masks,
                              "bee_center": bee_center,
                               "summed_mask": summed_mask,
                               "bee_sizes":bee_sizes}

            with open(f"{self.synthetic_data_directory}/annotations/full_annotations/synthetic_data_{synthetic_id}.pkl", 'wb') as f:
               pickle.dump(data_collection, f)

            with open(f"{self.synthetic_data_directory}/annotations/summed_mask_only/synthetic_data_summed_mask_{synthetic_id}.pkl", 'wb') as f:
               pickle.dump({"image_directory":f"{self.synthetic_data_directory}/images/{synthetic_id}.png","summed_mask": summed_mask}, f)

            pickle_file_paths_full_data.append(f"{self.synthetic_data_directory}/annotations/full_annotations/synthetic_data_{synthetic_id}.pkl")
            pickle_file_paths_truncated_data.append(f"{self.synthetic_data_directory}/annotations/summed_mask_only/synthetic_data_summed_mask_{synthetic_id}.pkl")

            print(f"image {synthetic_id} complete")

        data_collection = {"pickle_file_paths_full_data": pickle_file_paths_full_data, "pickle_file_paths_truncated_data":pickle_file_paths_truncated_data}

        with open(f"{self.synthetic_data_directory}/synthetic_data_pickle_paths.pkl", 'wb') as f:
            pickle.dump(data_collection, f)
