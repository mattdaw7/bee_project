import argparse

import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from tqdm import tqdm

from data_loader import bee_dataset
from loss_functions import FocalLoss, mIoULoss
from unet import UNet

from data_generator import data_generator

import matplotlib.pyplot as plt

import torch.nn.functional as F

def setup_training(args, model):
    criterion = nn.CrossEntropyLoss().float()
    if args.loss == 'focalloss':
        criterion = FocalLoss(gamma=3/4)
    elif args.loss == 'iouloss':
        criterion = mIoULoss(n_classes=2)
    elif args.loss == 'crossentropy':
        criterion = nn.CrossEntropyLoss()
    else:
        print('Loss function not found!')

    # specify optimizer (stochastic gradient descent) and learning rate = 0.01
    optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate)
    device = torch.device("cuda:0" if torch.cuda.is_available() and not(args.cpu_train) else "cpu")
    model = model.to(device)
    criterion = criterion.to(device)
    return model, criterion, optimizer, device

def train(args, model, train_loader, test_loader, n_epochs=20):
    # specify loss function (categorical cross-entropy)

    model, criterion, optimizer, device = setup_training(args, model)

    total_pixels = None
    for epoch in range(n_epochs):
        # monitor training loss
        train_loss = 0.0

        model.train()  # prep model for training

        with tqdm(train_loader, unit="batch") as tepoch:
            tepoch.set_description(f"Epoch {epoch}")
            for data, target in tepoch:

                if total_pixels is None:
                    total_pixels = np.prod([*data.shape])//3

                data = data.to(device)
                target = target.to(device)

                # clear the gradients of all optimized variables
                optimizer.zero_grad()
                # forward pass: compute predicted outputs by passing inputs to the model
                output = model(data)
                # calculate the loss
                loss = criterion(output, target)
                # # backward pass: compute gradient of the loss with respect to model parameters
                loss.backward()
                # # perform a single optimization step (parameter update)
                optimizer.step()
                # # update running training loss
                loss_item = loss.detach().item() * data.size(0)
                train_loss += loss_item

                accuracy = (total_pixels - torch.sum(abs(torch.argmax(data, axis=1) - torch.argmax(output, axis=1)))) / total_pixels

                tepoch.set_postfix(loss=loss_item, accuracy=accuracy)

        model_scripted = torch.jit.script(model)
        model_scripted.save('model_scripted.pt')

def sigmoid_focal_loss(inputs, targets, num_boxes, alpha: float = 0.25, gamma: float = 2):
    """
    Loss used in RetinaNet for dense detection: https://arxiv.org/abs/1708.02002.
    Args:
        inputs: A float tensor of arbitrary shape.
                The predictions for each example.
        targets: A float tensor with the same shape as inputs. Stores the binary
                 classification label for each element in inputs
                (0 for the negative class and 1 for the positive class).
        alpha: (optional) Weighting factor in range (0,1) to balance
                positive vs negative examples. Default = -1 (no weighting).
        gamma: Exponent of the modulating factor (1 - p_t) to
               balance easy vs hard examples.
    Returns:
        Loss tensor
    """
    prob = inputs.sigmoid()
    ce_loss = F.binary_cross_entropy_with_logits(inputs, targets, reduction="none")
    p_t = prob * targets + (1 - prob) * (1 - targets)
    loss = ce_loss * ((1 - p_t) ** gamma)

    if alpha >= 0:
        alpha_t = alpha * targets + (1 - alpha) * (1 - targets)
        loss = alpha_t * loss

    return loss.mean(1).sum() / num_boxes


def dice_loss(input, target, num_boxes):
    if not torch.is_tensor(input):
        raise TypeError("Input type is not a torch.Tensor. Got {}"
                        .format(type(input)))
    if not len(input.shape) == 4:
        raise ValueError("Invalid input shape, we expect BxNxHxW. Got: {}"
                         .format(input.shape))
    if not input.shape[-2:] == target.shape[-2:]:
        raise ValueError("input and target shapes must be the same. Got: {}"
                         .format(input.shape, input.shape))
    if not input.device == target.device:
        raise ValueError(
            "input and target must be in the same device. Got: {}".format(
                input.device, target.device))
    # compute softmax over the classes axis
    input_soft = F.softmax(input, dim=1)

    # create the labels one hot tensor
    # target_one_hot = one_hot(target, num_classes=input.shape[1],
    #                          device=input.device, dtype=input.dtype)

    # compute the actual dice score
    dims = (1, 2, 3)
    intersection = torch.sum(input_soft * target, dims)
    cardinality = torch.sum(input_soft + target, dims)

    dice_score = 2. * intersection / (cardinality + 1e5)
    return torch.mean(1. - dice_score) / num_boxes

def F_score(logit, label, threshold=0.5, beta=2):
    prob = torch.sigmoid(logit)
    prob = prob > threshold
    label = label > threshold

    TP = (prob & label).sum(1).float()
    TN = ((~prob) & (~label)).sum(1).float()
    FP = (prob & (~label)).sum(1).float()
    FN = ((~prob) & label).sum(1).float()

    precision = TP / (TP + FP + 1e-12)
    recall = TP / (TP + FN + 1e-12)
    F2 = (1 + beta**2) * precision * recall / (beta**2 * precision + recall + 1e-12)
    return F2.mean(0)

def grad_F_score(y_pred, label, beta=2):
    # y_pred = torch.sigmoid(logit)
    TP = (y_pred * label).sum(dim=1)
    FP = ((1 - y_pred) * label).sum(dim=1)
    FN = (y_pred * (1 - label)).sum(dim=1)
    fbeta = (1 + beta**2) * TP / ((1 + beta**2) * TP + (beta**2) * FN + FP + 1e-5)
    fbeta = fbeta.clamp(min=1e-5, max=1 - 1e-5)
    return 1 - fbeta.mean()

def dynamic_training(args, model, train_loader, max_bees_to_train=200):
    # specify loss function (categorical cross-entropy)

    model, criterion, optimizer, device = setup_training(args, model)

    criterion = nn.MSELoss()

    last_accuracy = None

    bees_to_train = 3

    losses = []
    accuracy_imporvements = []

    update_step = 0

    overlap_pixles = args.overlap_pixles

    l2_lambda = 0.001

    while max_bees_to_train > bees_to_train:
        train_loss = 0.0
        model.train()

        with tqdm(train_loader, unit="bees_used") as tepoch_bee_count:
            while bees_to_train < 300:
                data, target = train_loader.sample(args.batch, bees_to_train)

                mask = target['summed_mask']

                # figs, axs = plt.subplots(1,2)
                # axs[0].imshow(target[0])
                # axs[1].imshow(data[0].permute(1,2,0))
                # plt.show()

                total_pixels = np.prod([*mask.shape])

                data = data.to(device)
                mask = mask.to(device)

                # clear the gradients of all optimized variables
                optimizer.zero_grad()
                # forward pass: compute predicted outputs by passing inputs to the model
                b, p, c, h, w = data.shape
                output = model(data.flatten(end_dim=1))
                output = output.view((b, p, 2, h, w))
                if args.do_partition_image:
                    output = output[:,:,:,overlap_pixles:-overlap_pixles,overlap_pixles:-overlap_pixles]

                mask_stacked = torch.stack([mask,torch.logical_not(mask).float()], dim=-3)

                sig_loss = sigmoid_focal_loss(output, mask_stacked, bees_to_train)
                d_loss = dice_loss(output.flatten(end_dim=1), mask_stacked.flatten(end_dim=1), bees_to_train) * 1e4

                output_logits_last = output.permute(0,1,3,4,2)

                sofmax_guess = output_logits_last.softmax(dim=-1)
                flattened_softmax_guess = sofmax_guess.flatten(end_dim=-2)
                ground_truth_flattened = torch.flatten(mask)

                stacked_ground_truth = torch.stack([torch.logical_not(ground_truth_flattened), ground_truth_flattened]).T

                f_loss = 1-grad_F_score(flattened_softmax_guess, stacked_ground_truth)

                l2_norm = sum(p.pow(2.0).sum() for p in model.parameters())

                loss = torch.clamp(sig_loss, min=-1, max=1) + torch.clamp(l2_norm * l2_lambda, min=-.5, max=.5) + f_loss

                # loss = f_loss * 100

                # calculate the loss
                # loss = criterion(output_relative, mask.float())
                # # backward pass: compute gradient of the loss with respect to model parameters
                loss.backward()
                # # perform a single optimization step (parameter update)
                optimizer.step()
                # # update running training loss
                loss_item = loss.detach().item() * data.size(0)
                train_loss += loss_item

                accuracy = 1-((torch.sum(abs(1-torch.round(flattened_softmax_guess) - stacked_ground_truth ))) / (total_pixels*2)).item()

                update_step += 1

                if accuracy > .9:
                    if bees_to_train == 39:
                        update_step = 0
                    if bees_to_train < 40:
                        bees_to_train += 1
                    elif accuracy > .92:
                        if update_step > 100:
                            return "done"

                    model_scripted = torch.jit.script(model)
                    model_scripted.save(f'{args.model_save_path}/model_scripted_gpu.pt')



                    # if bees_to_train % 4 == 0:
                    #     for g in optimizer.param_groups:
                    #         g['lr'] *= 0.8

                if not(last_accuracy is None):
                    losses = []
                    accuracy_imporvements = []

                tepoch_bee_count.set_postfix(bees=bees_to_train, loss=loss_item, accuracy=accuracy, correct_num_forground_pixles=torch.mean(stacked_ground_truth[:,1]).item(), foreground_pixles=1-(torch.sum(output.argmax(dim=2) / total_pixels)))



# cows go moo
def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=str, default='./Semantic segmentation dataset', help='path to your dataset')
    parser.add_argument('--num_epochs', type=int, default=100, help='dnumber of epochs')
    parser.add_argument('--batch', type=int, default=1, help='batch size')
    parser.add_argument('--loss', type=str, default='iouloss', help='focalloss | iouloss | crossentropy')
    parser.add_argument('--model_save_path', default='../data/trained_models')
    parser.add_argument('--bee_cutouts', default='../data/bee_cutouts')
    parser.add_argument('--angle_file', default='../data/bee_angles/convertcsv.csv')
    parser.add_argument('--background', default='../data/background')
    parser.add_argument('--synthetic_data', default='../data/synthetic_data')
    parser.add_argument('--cut_images', default='../data/cut_images')
    parser.add_argument('--dynamic_train', default=True)
    parser.add_argument('--use_full_y_data', default=True)
    parser.add_argument('--learning_rate', default=.008)
    parser.add_argument('--cpu_train', default=True)
    parser.add_argument('--partition_window_sizes', default=600)
    parser.add_argument('--overlap_pixles', default=50)
    parser.add_argument('--do_partition_image', default=False)
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    image_processor = data_generator(args)

    image_processor.get_cutout_stats()

    # image_processor.preprocess_synthetic_data(4)

    train_dataset = bee_dataset(args,train=True, train_size=.66, progressive_training=args.dynamic_train, full_y_data=args.use_full_y_data)
    # test_dataset = bee_dataset(args, train=False, train_size=.66)

    model = UNet(n_channels=3, n_classes=2)

    model_scripted = torch.jit.script(model)
    model_scripted.save(f'{args.model_save_path}/model_scripted.pt')
    model = torch.jit.load(f'{args.model_save_path}/model_scripted.pt')

    if args.dynamic_train:
        test_dataloader = dynamic_training(args, model, train_dataset)
    else:
        train_loader = DataLoader(train_dataset, batch_size=2, shuffle=True)
        # test_loader = DataLoader(test_dataset, batch_size=2, shuffle=True)
        train(args, model, train_loader, None)
