import math
import pickle
from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms
from data_generator import data_generator
import torch

class bee_dataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, args, train=False, progressive_training=False, train_size=.66, full_y_data=False):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.do_partition_image = args.do_partition_image
        self.full_y_data = full_y_data
        if progressive_training:
            self.data_generator_object = data_generator(args)
        else:
            with open(f"{args.synthetic_data}/synthetic_data_pickle_paths.pkl", 'rb') as f:
                data = pickle.load(f)

            if self.full_y_data:
                pickle_file_paths = data['pickle_file_paths_full_data']
            else:
                pickle_file_paths = data['pickle_file_paths_truncated_data']

            num_samples = len(pickle_file_paths)
            train_samples_to_use = math.ceil(num_samples * train_size)

            if train:
                self.pickle_file_paths = pickle_file_paths[:train_samples_to_use]
            else:
                self.pickle_file_paths = pickle_file_paths[train_samples_to_use:]

            self.convert_tensor = transforms.ToTensor()

        self.partition_window_sizes = args.partition_window_sizes
        self.overlap_pixles = args.overlap_pixles

    def __len__(self):
        return len(self.pickle_file_paths)

    def partition_image(self, image, y_data):

        mask = y_data['summed_mask']

        _,_,x,y = image.shape

        x_corner = 0
        y_corner = 0
        
        boxes = []

        # partition_window_sizes = self.partition_window_sizes

        # boxes, x_left, x_right, y_top, y_bottom
        while x_corner < x - self.partition_window_sizes or y_corner < y - self.partition_window_sizes:
            further_x = min(x_corner+self.partition_window_sizes+self.overlap_pixles*2, x)
            further_y = min(y_corner + self.partition_window_sizes+self.overlap_pixles*2, y)
            boxes.append([max(further_x - self.partition_window_sizes - self.overlap_pixles * 2, 0), further_x,
                          max(further_y - self.partition_window_sizes - self.overlap_pixles * 2, 0), further_y])

            if x_corner+self.partition_window_sizes > x:
                x_corner = 0
                y_corner += self.partition_window_sizes - self.overlap_pixles
            else:
                x_corner += self.partition_window_sizes - self.overlap_pixles

        further_x = min(x_corner + self.partition_window_sizes + self.overlap_pixles * 2, x)
        further_y = min(y_corner + self.partition_window_sizes + self.overlap_pixles * 2, y)
        boxes.append([max(further_x - self.partition_window_sizes - self.overlap_pixles * 2, 0), further_x,
                      max(further_y - self.partition_window_sizes - self.overlap_pixles * 2, 0), further_y])

        image_set = []
        mask_set = []
        for box in boxes:
            image_set.append( image[:,:,box[0]:box[1],box[2]:box[3]] )
            # noinspection PyTypeHints
            mask_set.append(mask[:,box[0]+self.overlap_pixles:box[1]-self.overlap_pixles, box[2]+self.overlap_pixles:box[3]-self.overlap_pixles])

        y_data['summed_mask'] = torch.stack(mask_set)

        y_data['partition_set'] = mask_set

        return torch.stack(image_set), y_data

    def unsqueeze_data(self, image, y_data):
        image = image.unsqueeze(0)
        y_data['summed_mask'] = y_data['summed_mask'].unsqueeze(0)
        return image, y_data

    def sample(self, batch, bees_to_train):
        if self.full_y_data:
            background_image, y = self.data_generator_object.generate_data_for_image(
                batch, bees_to_train, self.full_y_data)
        else:
            background_image, y = self.data_generator_object.generate_data_for_image(batch, bees_to_train, self.full_y_data)

        if self.do_partition_image:
            background_image, y = self.partition_image(background_image, y)
        else:
            background_image, y = self.unsqueeze_data(background_image, y)

        return background_image, y

    def evolve_input(self, target):

        background_image, y = self.data_generator_object.evolve_input(target)

        # if self.do_partition_image:
        #     background_image, y = self.partition_image(background_image, y)
        # else:
        #     background_image, y = self.unsqueeze_data(background_image, y)

        background_image = background_image.unsqueeze(0)

        return background_image, y

    def __getitem__(self, idx):

        with open(self.pickle_file_paths[idx], 'rb') as f:
            data = pickle.load(f)

        if self.full_y_data:
            y = { 'bee_center':data['bee_center'], 'individual_bee_masks':data['individual_bee_masks'], 'bee_sizes':data['bee_sizes'] }
        else:
            y = data['summed_mask']

        image_directory = data['image_directory']
        image = self.convert_tensor(Image.open(image_directory))[:3,:,:]
        return image, y



