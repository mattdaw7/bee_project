import argparse
from data_generator import data_generator

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--bee_cutouts', default='../data/bee_cutouts')
    parser.add_argument('--background', default='../data/background')
    parser.add_argument('--synthetic_data', default='../data/synthetic_data')
    parser.add_argument('--cut_images', default='../data/cut_images')
    parser.add_argument('--num_images_to_create', default=100)
    return parser.parse_args()

if __name__ == '__main__':
    args = get_args()
    image_processor = data_generator(args.synthetic_data)
    image_processor.preprocess_synthetic_data(args.bee_cutouts, args.background, args.cut_images, args.num_images_to_create)