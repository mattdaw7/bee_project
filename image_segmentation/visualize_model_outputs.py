import argparse

import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from tqdm import tqdm

from data_loader import bee_dataset
from loss_functions import FocalLoss, mIoULoss
from unet import UNet

from data_generator import data_generator

import matplotlib.pyplot as plt

from matplotlib.patches import Arrow, Circle

import time

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=str, default='./Semantic segmentation dataset', help='path to your dataset')
    parser.add_argument('--num_epochs', type=int, default=100, help='dnumber of epochs')
    parser.add_argument('--batch', type=int, default=2, help='batch size')
    parser.add_argument('--loss', type=str, default='iouloss', help='focalloss | iouloss | crossentropy')
    parser.add_argument('--model_save_path', default='../data/trained_models')
    parser.add_argument('--bee_cutouts', default='../data/bee_cutouts')
    parser.add_argument('--angle_file', default='../data/bee_angles/convertcsv.csv')
    parser.add_argument('--background', default='../data/background')
    parser.add_argument('--synthetic_data', default='../data/synthetic_data')
    parser.add_argument('--cut_images', default='../data/cut_images')
    parser.add_argument('--dynamic_train', default=True)
    parser.add_argument('--use_full_y_data', default=True)
    parser.add_argument('--learning_rate', default=.008)
    parser.add_argument('--cpu_train', default=True)
    parser.add_argument('--partition_window_sizes', default=500)
    parser.add_argument('--overlap_pixles', default=50)
    parser.add_argument('--do_partition_image', default=False)
    return parser.parse_args()

def get_contours(mask):
    y_mask, x_mask = np.where(mask > 0)

    unexplored_points = [y_mask, x_mask]

    points = set()
    for i in range(len(y_mask)):
        points.add((y_mask[i], x_mask[i]))

    def get_neighbors(point):
        new_points = []
        x, y = point

        values_to_extend = np.arange(12)+1

        for value in values_to_extend:
            if (x+value,y) in points:
                new_points.append([x+value,y])
                points.remove((x+value,y))
            if (x,y+value) in points:
                new_points.append([x,y+value])
                points.remove((x,y+value))
            if (x-value,y) in points:
                new_points.append([x-value,y])
                points.remove((x-value,y))
            if (x,y-value) in points:
                new_points.append([x,y-value])
                points.remove((x,y-value))
            
            if (x+value,y+value) in points:
                new_points.append([x+value,y+value])
                points.remove((x+value,y+value))
            if (x-value,y+value) in points:
                new_points.append([x-value,y+value])
                points.remove((x-value,y+value))
            if (x-value,y-value) in points:
                new_points.append([x-value,y-value])
                points.remove((x-value,y-value))
            if (x+value,y-value) in points:
                new_points.append([x+value,y-value])
                points.remove((x+value,y-value))


        return new_points

    completed_clusters = []

    while len(points) > 0:
        new_point = points.pop()

        processing_cluster_points = [new_point]
        output_cluster_points = [new_point]
        while len(processing_cluster_points) > 0:
            exploring_point = processing_cluster_points.pop(0)
            found_points = get_neighbors(exploring_point)
            processing_cluster_points += found_points
            output_cluster_points += found_points
        if len(output_cluster_points) > 30:
            completed_clusters.append(output_cluster_points)

    return completed_clusters

def visualize_data(x, mask, target):
    # fig, ax = plt.subplots(1,3, figsize=(10,10))
    masked_image = x.clone().permute(1,2,0)
    partitioned_mask_image = x.clone().permute(1,2,0)
    # ax[0].imshow(x.permute(1,2,0))

    # plot full mask
    color = list(np.random.choice(range(256), size=3) / 256)
    y_mask, x_mask = np.where(mask > 0)
    # masked_image[y_mask, x_mask, :] = torch.tensor(color).float()
    # masked_image = masked_image.permute(2, 0, 1)
    # ax[1].imshow(masked_image)

    centers = []

    # plot segmented_mask
    start_time = time.time()
    contours = get_contours(mask)
    end_time = time.time() - start_time


    for con in contours:
        color = list(np.random.choice(range(256), size=3) / 256)
        con = np.array(con)
        centers.append(np.mean(con, axis=0))
        center = np.mean(con, axis=0)
        # ax[0].add_patch(Circle((center[1],center[0]), radius=4,color='red'))
        # partitioned_mask_image[con[:,0], con[:,1], :] = torch.tensor(color).float()
        centers.append(center)
    # ax[2].imshow(partitioned_mask_image)
    # plt.show()

    # metrics
    true_y_mask, true_x_mask = np.where(target['summed_mask'][0][0] > 0)
    mask_accuracy = torch.sum(masked_image[true_y_mask, true_x_mask, :]) / np.prod(
        [*masked_image[true_y_mask, true_x_mask, :].shape])

    centers = np.array(centers)
    actual_num_bees = len(target['individual_bee_masks'])
    individual_masks_special_summed = torch.sum((target['individual_bee_masks'].T * (torch.arange(len(target['individual_bee_masks']))+1)).T, axis=0)
    cluster_assignments = individual_masks_special_summed[centers[:,0].astype(int),centers[:,1].astype(int)]
    total_clusters = len(centers)
    background_misses = len(np.where(cluster_assignments == 0)[0])
    double_clusters = total_clusters - len(np.unique(cluster_assignments))
    # print(double_clusters)
    correct_assignment_percentages = 1 - ((actual_num_bees - (total_clusters - background_misses - double_clusters)) / actual_num_bees)
    # print(correct_assignment_percentages)

    return correct_assignment_percentages, mask_accuracy, end_time


def recombine(data, output, target):
    x, y = [1100, 1650]



    print("think")

if __name__ == '__main__':
    args = get_args()

    train_dataset = bee_dataset(args,train=True, train_size=.66, progressive_training=args.dynamic_train, full_y_data=args.use_full_y_data)
    # test_dataset = bee_dataset(args, train=False, train_size=.66)

    model = UNet(n_channels=3, n_classes=2)

    model_scripted = torch.jit.script(model)
    model = torch.jit.load(f'{args.model_save_path}/model_scripted.pt')

    assignments = []

    masks = []

    times = []
    time_partition = []

    for i in range(50):

        data, target = train_dataset.sample(1, 150)

    # data, target = train_dataset.evolve_input(target)

        b, p, c, h, w = data.shape
        start_time = time.time()
        output = model(data.flatten(end_dim=1))
        total_time = time.time() - start_time

        # recombine(data, output, target)

        output = output.view((b, p, 2, h, w))
        output_discretized = output.min(dim=2)[1]
        correct_assignment_percentages, mask_accuracy, end_time = visualize_data(data[0][0], output_discretized[0][0], target)
        assignments.append(correct_assignment_percentages)
        masks.append(mask_accuracy)
        times.append(total_time)
        time_partition.append(end_time)

        print(i)

    print(np.mean(assignments), np.std(assignments))

    print(np.mean(masks), np.std(masks))

    print(np.mean(times), np.std(times))

    print(np.mean(time_partition), np.std(time_partition))